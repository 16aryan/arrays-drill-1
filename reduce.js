function reduce(elements, cb, startingValue) {
    // Do NOT use .reduce to complete this function.
    // How reduce works: A reduce function combines all elements into a single value going from left to right.
    // Elements will be passed one by one into `cb` along with the `startingValue`.
    // `startingValue` should be the first argument passed to `cb` and the array element should be the second argument.
    // `startingValue` is the starting value.  If `startingValue` is undefined then make `elements[0]` the initial value.
   if(!startingValue){
startingValue = 0;
   } 
   if(!Array.isArray(elements)){
    return undefined;
}
let sum=0;
for(let index= startingValue;index<elements.length;index++){
    sum+=cb(index,elements);
}
console.log(sum);

}
function cb(index,elements){
    return elements[index];
}
//let arr= [1,2,3,4,5];
//reduce(arr,cb,'3');
module.exports ={reduce,cb};
