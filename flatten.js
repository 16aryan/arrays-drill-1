function flatten(arr) {
    // Flattens a nested array (the nesting can be to any depth).
    // Hint: You can solve this using recursion.
    // Example: flatten([1, [2], [3, [[4]]]]); => [1, 2, 3, 4];
    if(!Array.isArray(arr)){
        return console.log(undefined);
    }
    for(const index of arr){
        
        if(Array.isArray(index)){
          
            let newArr = index;
            flatten(newArr);
        }
        else{
            console.log(index);
        }
}
}
module.exports = flatten;