function find(arr, cb, indexToBeFound) {
  let found = false;
  if(!Array.isArray(arr)){
    return undefined;
}
    for(let index =0;index<arr.length;index++){
        if(Array.isArray(arr[index])){
        let newArr = arr[index];
        for(let innerIndex=0;innerIndex<newArr.length;innerIndex++){
         found= cb(newArr[innerIndex],indexToBeFound);
         if(found===true){
            return true;
         }
        }
       // print(newArr);
        }
        else{
            found= cb(arr[index],indexToBeFound);
            if(found===true){
                return true;
             }
        }
    }
    return false;
}
function cb(element,indexToBeFound){
   if(element===indexToBeFound){
    return true;
   }
   return false;
}
//let arr= [1,2,3,[4],5];

//console.log(find(arr,cb,4));
module.exports = {find,cb};