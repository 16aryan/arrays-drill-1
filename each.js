function each(arrayTest,cb){
    if(!Array.isArray(arrayTest)){
        return undefined;
    }
for(let index = 0;index<arrayTest.length;index++){
   cb(arrayTest[index],index);
}
}
function cb(element, index){
    console.log(`${index} ${element}`);
}
//const arr =[1,2,3,4,5];
//each(arr,cb);
module.exports = {each,cb};