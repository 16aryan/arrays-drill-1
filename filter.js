function filter(elements, cb, indexToBeFound) {
    // Do NOT use .includes, to complete this function.
    // Look through sd value in `elements` and pass each element to `cb`.
    // If `cb` returns `true` then return that element.
    // Return `undefined` if no elements pass the truth test.
if(!indexToBeFound){
    return console.log(undefined);
}
if(!Array.isArray(elements)){
    return undefined;
}
let result = false;
for(let index=0;index<elements.length;index++){
    result =  cb(elements[index],indexToBeFound);
    if(result){
        return console.log(`${index}`);
    }
}

return console.log('undefined');


}
function cb(element,indexToBeFound){
    if(element == indexToBeFound){
        return true;
    }
    else{
        return false;
    }
}
//let arr= [1,2,3,4,5];
//ilter(arr,cb,4);
module.exports = {filter,cb};